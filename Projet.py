# -*- coding: utf-8 -*-
"""
Created on Tue Nov 29 18:43:06 2022

@author: guill
"""

def SuffixArray(a):
    """

    Parameters
    ----------
    a : string
        input string we perform suffix array on

    Returns
    -------
    result : array of int
        represents the indexes of the lexicographically sorted suffixes

    """
    result=[len(a)-2]
    for i in range(len(a)-2):
        for j in range(len(a)-2):
            if(j not in result):
                result.append(j)
                break
        for j in range(result[i+1],len(a)-2):
            if(j not in result):
                if(a[result[i+1]+1:]>a[j+1:]):
                    result[i+1]=j
    result.append(-1)
    return result
    
def LCP(a, b):
    """

    Parameters
    ----------
    a : string
        string we compare with b
    b : string
        string we compare with a

    Returns
    -------
    count : int
        count of the number of consecutive equal characters between a and b

    """
    count=0
    for i in range(min(len(a),len(b))):
        if(a[i]=="$" or b[i]=="$"):
            break
        elif(not a[i]==b[i]):
            break
        count+=1
    return count

def SASimpleSearch(a, sub):
    """

    Parameters
    ----------
    a : string
        string we perform Simple Search on
    sub : string
        pattern we try to find in a

    Returns
    -------
    d : int
    f : int
        index where the pattern starts in the input string

    """
    d=-1
    f=len(a)-2
    SA=SuffixArray(a)
    while(d+1<f):
        i=(d+f)//2
        L=a[SA[i+1]+1:len(a)]
        l=LCP(L,sub)
        if(l==len(sub)):
            f=i
        elif(L[l]>sub[l]):
            f=i
        else:
            d=i
    return (d,f)

def Search(a, sub):
    """

    Parameters
    ----------
    a : string
        string we perform Search on
    sub : string
        pattern we try to find in a

    Returns
    -------
    int
        index where the pattern starts in the input string

    """
    SA=SuffixArray(a)
    n, m = len(a)-2,len(sub)
    (d, ld), (f, lf) = (-1, 0), (n, 0)
    while(d+1<f):
        i = (d + f) // 2
        Li = a[SA[i+1]+1:][:-1]
        lcp1=LCP(a[SA[i+1]+1:],a[SA[f+1]+1:])
        lcp2=LCP(a[SA[d+1]+1:],a[SA[i+1]+1:])
        if ld <= lcp1 and lcp1 < lf:
            (d, ld) = (i, lcp1)
        elif ld <= lf and lf < lcp1:
            f = i
        elif lf <= lcp2 and lcp2 < ld:
            (f, lf) = (i, lcp2)
        elif lf <= ld and ld < lcp2:
            d = i
        else:
            l = max(ld, lf)
            l += LCP(sub[l:], Li[l:])
            if l == m and l == len(Li):
                return i
            elif (l == len(Li)) or (l != m and Li[l] < sub[l]):
                (d, ld) = (i, l)
            else:
                (f, lf) = (i, l)
    return (d,f)

if __name__=='__main__':
    print("Hello and welcome to Text Pattern project computing the Suffix Array, Simple Search (on SA) and Search (on SA).\nWhat do you want do to ?\n1: Suffix Array\n2: Simple Search\n3: Search\nhelp: get informations on how to use the software\nquit: quit the software")
    inp=""
    while(inp!="quit"):
        inp=input(": ")
        while(inp!="1" and inp!="2" and inp!="3" and inp!="help" and inp!="quit"):
            inp=input("Please provide a valid choice: ")
        if(inp=="quit"):
            break
        elif(inp=="help"):
            print("In this software you can use three different algorithms, Suffix Array that returns an array of the index of the lexicographically sorted suffixes of the input string, Simple Search implements this algorithm on the SA of the input string and returns the position of one the occurence of the pattern, Search implements this algorithm on the SA of the input string and returns the position of one the occurence of the pattern\n\n1: Suffix Array\n2: Simple Search\n3: Search\nquit: quit the software")
        else:
            inpbis=input("Provide an input string to perform Suffix Array, Simple Search or Search on: ")
            if(len(inpbis)>0 and inpbis[0]!="#"):
                inpbis="#"+inpbis
            else:
                inpbis="#"
            if(inpbis[len(inpbis)-1]!="$"):
                inpbis+="$"
            if(inp=="1"):
                print("The result of the Suffix Array is "+str(SuffixArray(inpbis)))
            else:
                pattern=input("Provide the pattern to look for in the input string: ")
                if(inp=="2"):
                    print("The result of the Simple Search is "+str(SASimpleSearch(inpbis,pattern)))
                else:
                    print("The result of the Search is "+str(Search(inpbis,pattern)))    